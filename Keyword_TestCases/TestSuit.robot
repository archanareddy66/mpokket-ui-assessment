*** Settings ***
Test Teardown     Close Browser
Resource          ../Global/super.robot

*** Test Cases ***
TC-01 Search a Product and Validate Search Results
    [Setup]    Read TestData From Excel    TC_01    ProductDetails
    Comment     Launch Browser and Navigate to Amazon website
    Launch Browser and Navigate to URL    ${URL}    ${BROWSER_NAME}
    Comment    Search for a product in search bar
    Search for a Product    ${test_data}[ProductName]
    Comment    Validate Search Results
    Validate Search Results

TC-02 Add a Product to the Cart and Validate whether the Added Product is in the cart
    [Setup]    Read TestData From Excel    TC_02    ProductDetails
    Comment     Launch Browser and Navigate to Amazon website
    Launch Browser and Navigate to URL    ${URL}    ${BROWSER_NAME}
    Comment    Search for a product in search bar
    Search for a Product    ${test_data}[ProductName]
    Comment    Validate Search Results
    Validate Search Results
    Comment    Add a product to Cart
    Add Product to Cart    ${test_data}[ProductName]    ${test_data}[Quantity]
    Comment    Validate Success message after adding product to cart
    Validate Added Product in Cart    ${test_data}[SuccessMessage1]

TC-03 Increase product quantity and Validate change in the price
    [Setup]    Read TestData From Excel    TC_03    ProductDetails
    Comment     Launch Browser and Navigate to Amazon website
    Launch Browser and Navigate to URL    ${URL}    ${BROWSER_NAME}
    Comment    Search for a product in search bar
    Search for a Product    ${test_data}[ProductName]
    Comment    Validate Search Results
    Validate Search Results
    Comment    Add a product to Cart
    Add Product to Cart    ${test_data}[ProductName]    ${test_data}[Quantity]
    Comment    Validate Success message after adding product to cart
    Validate Added Product in Cart    ${test_data}[SuccessMessage1]
    Comment    Validate price in cart
    Validate Product Price in Cart

TC-04 Remove product from the cart and Validate change in the cart after removing product
    [Setup]    Read TestData From Excel    TC_04    ProductDetails
    Comment     Launch Browser and Navigate to Amazon website
    Launch Browser and Navigate to URL    ${URL}    ${BROWSER_NAME}
    Comment    Search for a product in search bar
    Search for a Product    ${test_data}[ProductName]
    Comment    Validate Search Results
    Validate Search Results
    Comment    Add a product to Cart
    Add Product to Cart    ${test_data}[ProductName]    ${test_data}[Quantity]
    Comment    Validate Success message after adding product to cart
    Validate Added Product in Cart    ${test_data}[SuccessMessage1]
    Comment    Validate product price in cart
    Validate Product Price in Cart
    Comment    Remove Product From Cart
    Remove Product From Cart
    Comment    Validate Product is Removed from the Cart
    Validate Product is Removed from the Cart    ${test_data}[SuccessMessage2]
