*** Settings ***
Resource          ../Global/super.robot

*** Keywords ***
Search for a Product
    [Arguments]    ${search_text}
    Wait Until Element Is Visible    ${textbox.homepage.searchbar}    ${MEDIUM_WAIT}    Searchbar is not visible after waiting ${MEDIUM_WAIT}
    Input Text    ${textbox.homepage.searchbar}    ${search_text}
    Click Element    ${button.homepage.search}

Validate Search Results
    ${status}    Run Keyword And Return Status    Scroll Element Into View    //span[normalize-space(text())="${test_data}[ProductName]"]
    Run Keyword If    ${status}==False    Fail And Take Screenshot    Searched product is not present in search results
    ${product_name}    Get Text    //span[normalize-space(text())="${test_data}[ProductName]"]
    Set Global Variable    ${product_name}

Add Product to Cart
    [Arguments]    ${product_name}    ${product_quantity}
    Wait Until Element Is Visible    //span[normalize-space(text())="${product_name}"]    ${SHORT_WAIT}    Searched text is not visible after waiting ${SHORT_WAIT}
    Click Element    //span[normalize-space(text())="${product_name}"]
    Switch Window    New
    Wait Until Element Is Visible    ${button.addtocart}
    Single Product Price
    Increase Product Quantity    ${product_quantity}
    Wait Until Element Is Visible    ${button.addtocart}    ${MEDIUM_WAIT}    Add To Cart button is not visible after waiting ${MEDIUM_WAIT}
    Click Element    ${button.addtocart}

Validate Added Product in Cart
    [Arguments]    ${added_to_cart_msg}
    Wait Until Element Is Visible    ${label.successmessage.addedtocart}    ${MEDIUM_WAIT}    Product is not added to cart
    ${result}    Get Text    ${label.successmessage.addedtocart}
    ${status}    Run Keyword And Return Status    Should Be Equal    ${result}    ${added_to_cart_msg}
    Run Keyword If    ${status}==False    Fail And Take Screenshot    Product is not added to cart

Increase Product Quantity
    [Arguments]    ${product_quantity}
    Wait Until Element Is Visible    ${dropdown.product.quantity}
    Select From List By Label    ${dropdown.product.quantity}    ${product_quantity}

Navigate to Cart Page
    Wait Until Element Is Visible    ${button.cartcount}    ${SHORT_WAIT}    Cart count button is not visible after waiting ${SHORT_WAIT}
    Click Element    ${button.cartcount}

Remove Product From Cart
    Navigate to Cart Page
    Wait Until Element Is Visible    ${label.cart.shoppingcart}    ${MEDIUM_WAIT}    Shopping Cart label is not visible after waiting ${MEDIUM_WAIT}
    Wait Until Element Is Visible    //span[normalize-space(text())="${product_name}"]    ${MEDIUM_WAIT}    Product Title is not visible after waiting ${MEDIUM_WAIT}
    Wait Until Element Clickable    //span[normalize-space(text())="${product_name}"]/../../../../following-sibling::div/span/following-sibling::span[@data-action="delete"]

Validate Product is Removed from the Cart
    [Arguments]    ${cart_is_empty_msg}
    Sleep    4
    Wait Until Element Is Visible    ${label.successmsg.amazoncart_is_empty}    ${MEDIUM_WAIT}    Your Amazon Cart is Empty message is not displayed after waiting Product is not visible after waiting ${MEDIUM_WAIT}
    ${result}    Get Text    ${label.successmsg.amazoncart_is_empty}
    ${status}    Run Keyword And Return Status    Should Be Equal    ${result}    ${cart_is_empty_msg}
    Run Keyword If    ${status}==False    Wait Until Element Is Visible    //span[normalize-space(text())="${product_name}"]    ${MEDIUM_WAIT}    Product is not visible after waiting ${MEDIUM_WAIT}

Single Product Price
    ${single_product_price}    Get Text    ${label.single_product_price}
    Set Global Variable    ${single_product_price}

Validate Product Price in Cart
    Navigate to Cart Page
    Wait Until Element Is Visible    ${label.multiple_product_price}    ${MEDIUM_WAIT}    Price is not visible after waiting ${MEDIUM_WAIT}
    ${multiple_product_price}    Get Text    ${label.multiple_product_price}
    Set Global Variable    ${multiple_product_price}
    ${status}    Run Keyword And Return Status    Should Not Be Equal    ${single_product_price}    ${multiple_product_price}
    Run Keyword If    ${status}==False    Should Be Equal    ${single_product_price}    ${multiple_product_price}

Validate Product Title
    Wait Until Element Is Visible    //span[@id='productTitle']
    ${product_title}    Get Text    //span[@id='productTitle']
    ${status}    Run Keyword And Return Status    Should Contain    ${product_name}    ${product_title}
    Run Keyword If    ${status}==False    Fail And Take Screenshot
