*** Settings ***
Resource          ../Global/super.robot

*** Keywords ***
Launch Browser
    [Arguments]    ${browser_name}    ${url}
    Run Keyword If    '${browser_name}'=='Chrome' or '${browser_name}'=='chrome' or '${browser_name}'=='gc'    Open Browser    ${url}    Chrome
    Run Keyword If    '${browser_name}'=='Firefox' or '${browser_name}'=='firefox' or '${browser_name}'=='ff'    Open Browser    ${url}    Firefox

Launch Browser and Navigate to URL
    [Arguments]    ${url}    ${browser_name}
    ${session}    Run Keyword And Return Status    Get Session Id
    Run Keyword If    ${session}==True    Go To    ${url}
    ...    ELSE    Launch Browser    ${browser_name}    ${url}
    Maximize Browser Window

Read TestData From Excel
    [Arguments]    ${testcaseid}    ${sheet_name}
    [Documentation]    Read TestData from excel file for required data.
    ...
    ...    Example:
    ...    Read TestData From Excel TC_01 SheetName
    ${test_data}    CustomLibrary.Get Ms Excel Row Values Into Dictionary Based On Key    ${TESTDATA_FOLDER}/TestData.xlsx    ${testcaseid}    ${sheet_name}
    Set Test Variable    ${test_data}

Take Screenshot
    Capture Page Screenshot

Set Browser Position
    [Arguments]    ${browser_name}
    Run Keyword If    '${browser_name}'=='Chrome' or '${browser_name}'=='chrome' or '${browser_name}'=='gc'    Set Window Position    0    5
    Run Keyword If    '${browser_name}'=='Firefox' or '${browser_name}'=='firefox' or '${browser_name}'=='ff'    Set Window Position    1005    6
    Set Window Size    959    1047

Create Screenshot Directory
    Run Keyword And Ignore Error    Create Directory    ${EXECDIR}/Screenshots
    SeleniumLibrary.Set Screenshot Directory    ${EXECDIR}/Screenshots

Navigate to Menu
    [Arguments]    ${menu}    ${sub_menu}
    Wait Until Time    4
    Wait Until Element Is Visible    ${link.homepage.home}    ${LONG_WAIT}    Home page is not visible after waiting ${LONG_WAIT}
    Wait Until Element Is Visible    //p[text()='${menu}']/parent::a    ${LONG_WAIT}    ${menu} menu item is not visible in home page
    Click Element    //p[text()='${menu}']/parent::a
    Wait Until Element Is Visible    //a[text()='${sub_menu}']    ${LONG_WAIT}    ${sub_menu} is not visible in ${menu} menu list
    Click Element    //a[text()='${sub_menu}']
    Wait Until Time    4
    Wait Until Element Is Visible    ${labels.home.submenu.title}    ${LONG_WAIT}    ${sub_menu}
    ${text}    Get Text    ${labels.home.submenu.title}
    Should Contain    ${text}    ${sub_menu}    ${sub_menu} page is not opening    ignore_case=True
    Wait Until Time    4

Fail And Take Screenshot
    [Arguments]    ${error_message}
    Run Keyword And Continue On Failure    Fail    ${error_message}
    Take Screenshot
