*** Variables ***
${label.successmessage.addedtocart}    //h1[contains(text(),'Added to Cart')]
${label.successmsg.amazoncart_is_empty}    //h2[@class='sc-your-amazon-cart-is-empty']
${label.single_product_price}    //span[@id='priceblock_ourprice']
${label.multiple_product_price}    //span[@id='sc-subtotal-amount-activecart']
${label.cart.shoppingcart}    //h1[normalize-space(text())='Shopping Cart']
${label.cart.youritems}    //h2[normalize-space(text())='Your Items']
