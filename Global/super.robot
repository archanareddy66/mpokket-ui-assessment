*** Settings ***
Library           String
Library           Collections
Resource          ../Object_Repository/buttons.robot
Resource          ../Object_Repository/dropdowns.robot
Resource          ../Object_Repository/textboxes.robot
Resource          ../Keywords/common.robot
Resource          global_variables.robot
Resource          ../Keywords/amazon.robot
Library           ../Library/CustomLibrary.py
Library           SeleniumLibrary
Resource          ../Object_Repository/labels.robot
